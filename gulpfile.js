var gulp           =  require('gulp');
var cssmin         =  require('gulp-cssmin');
var rename         =  require('gulp-rename');
var sass           =  require('gulp-sass');
var notify         =  require("gulp-notify");
var concat         =  require('gulp-concat');
var uglify         =  require('gulp-uglify');
var browserSync    =  require('browser-sync').create();
var htmlrender     =  require('gulp-htmlrender');
var watch          =  require('gulp-watch');

gulp.task('render', function() {
    return gulp.src('views/*.html', {read: false})
        .pipe(htmlrender.render())
        .pipe(gulp.dest('./'));
         browserSync.reload();
});

gulp.task('sass', function () {
  return gulp.src('styles/style.scss')
    .pipe(sass().on('error', function(err) {
            return notify().write(err); 
        }))
    .pipe(gulp.dest('content/css'))
});


gulp.task('minify-css', function () {
    gulp.src('content/css/style.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('content/css/'))
        //.pipe(notify({ message: 'CSS Minified' }));
        browserSync.reload();
});


gulp.task('serve', function () {

    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('watch',function(){
	    gulp.watch('styles/*.scss',['sass']);
        gulp.watch('content/css/style.css',['minify-css']);
          gulp.watch(['views/**/*.html'], ['render']);
});

gulp.task('default', ['sass','minify-css','render', 'serve', 'watch']);
