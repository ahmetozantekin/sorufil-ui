document.addEventListener('DOMContentLoaded', function () {

  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
  if ($navbarBurgers.length > 0) {
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', function () {
        var target = $el.dataset.target;
        var $target = document.getElementById(target);
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');
      });
    });
  }

  var $modalopen = document.getElementById('modal-open');
  var $modalopen2 = document.getElementById('modal-open2');
  
  $modalopen.addEventListener('click', function () {
    document.getElementById('main-question-modal').style.display = "block";
  })

  $modalopen2.addEventListener('click', function () {
    document.getElementById('main-question-modal').style.display = "block";
  })
  var $modalclose = document.getElementsByClassName('modal-close')[0];
   $modalclose.addEventListener('click', function () {
    document.getElementById('main-question-modal').style.display = "none";
  })



});


function detailAnswer(){
   var editor =document.getElementById('answer-text-editor');
   editor.classList.toggle('open-editor')
}